Este estudo visa medir se, de fato, a empregabilidade é um fator decisivo
sobre a evasão nas graduações da UFPB. Isso foi feito por meio da análise de dados
dos centros universitários, tendo em conta as taxas de conclusão
curso por centro e o desempenho profissional desses egressos.